package controllers

import (
	"backend/modules/db"
	"log"

	"github.com/gin-gonic/gin"
)

// Faq data
func Faq(c *gin.Context) {
	DB := db.InitDB()
	defer DB.Close()

	sql := "select id,title,content from faq order by scope,created_at"
	rows, err := DB.Query(sql)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	ret := db.ToObject(rows)
	c.JSON(200, gin.H{
		"status": "200",
		"data":   ret,
	})
}
