package controllers

import (
	"backend/modules/db"
	"log"

	"github.com/gin-gonic/gin"
)

// Banners data
func Banners(c *gin.Context) {
	DB := db.InitDB()
	defer DB.Close()

	sql := "select id,title,images,url from banners where is_visible = ? order by scope,created_at"
	rows, err := DB.Query(sql, 1)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	ret := db.ToObject(rows)
	c.JSON(200, gin.H{
		"status": "200",
		"data":   ret,
	})
}
