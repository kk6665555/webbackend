package controllers

import (
	"backend/modules/db"
	"log"
	"math"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql" // Import the mysql driver.
)

// New pagination url quary
type New struct {
	Page     int64 `json:"page"`
	Category int64 `json:"category"`
	ID       int64 `json:"id"`
	Agent    int64 `json:"agent"`
}

var m = make(map[string]interface{})

// News data
func News(c *gin.Context) {
	DB := db.InitDB()
	defer DB.Close()
	var new New
	new.Page = 1
	new.Category = 1
	new.Agent = 1
	c.ShouldBindJSON(&new)
	var count int
	if new.ID == 0 {
		err := DB.QueryRow("select count(*) as count from news where is_visible = ? and category_id = ? and agent = ? and start_time < NOW() and end_time > NOW()", 1, new.Category, new.Agent).Scan(&count)
		pageCount := float64(count) / 4
		pageall := int(math.Ceil(pageCount))
		sql := "select id, title,category_id,tagtitle,title,content,redirect_url,start_time,end_time,created_at from news where is_visible  = ? and category_id = ? and agent = ? and start_time < NOW() and end_time > NOW()  order by scope,created_at asc limit 4 offset ?"
		n := (new.Page - 1) * 4
		rows, err := DB.Query(sql, 1, new.Category, new.Agent, n)
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
		ret := db.ToObject(rows)
		c.JSON(200, gin.H{
			"status":    "200",
			"data":      ret,
			"total":     count,
			"page":      new.Page,
			"pageCount": pageall,
			"category":  new.Category,
		})
	} else {
		sql := "select id, title,category_id,tagtitle,title,content,redirect_url,start_time,end_time,created_at from news where id = ?  order by scope,id asc"
		rows, err := DB.Query(sql, new.ID)
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
		ret := db.ToObjectRow(rows)
		c.JSON(200, gin.H{
			"status": "200",
			"data":   ret,
		})
	}

}
