package controllers

import (
	"backend/modules/db"
	"backend/modules/db/config"
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
)

// URL apk web type
type URL struct {
	URL  int64 `json:"url"`
	Os   int64 `json:"os"`
	Type int64 `json:"type"`
}

// DefaultURL apk web url
func DefaultURL(c *gin.Context) {
	// cfg := config.Database{
	// 	Host: "127.0.0.1",
	// 	Port: "3306",
	// 	User: "",
	// 	Pwd:  "",
	// 	Name: "ttf_file",
	// }
	cfg := config.Database{
		Host: "127.0.0.1",
		Port: "3306",
		User: "root",
		Pwd:  "",
		Name: "ttf_file",
	}
	DB, err := sql.Open("mysql", cfg.GetDSN())

	if err != nil {
		_ = DB.Close()
		panic(err)
	}
	defer DB.Close()
	res := make(map[string]interface{}, 0)
	appstore := downloadURL(0, 2, DB)
	googlepaly := downloadURL(1, 3, DB)
	apk := downloadURL(1, 6, DB)

	res["appstore"] = appstore
	res["googlepaly"] = googlepaly
	res["apk"] = apk
	c.JSON(200, gin.H{
		"status": "200",
		"data":   res,
	})

}

// downloadUrl apk web url
func downloadURL(agent int, category int, DB *sql.DB) map[string]interface{} {

	sql := "select url from extension where os = ? and type = ?"
	rows, err := DB.Query(sql, agent, category)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	res := db.ToObjectRow(rows)
	return res

}
