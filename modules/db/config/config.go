package config

import (
	"encoding/json"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Database is a type of database connection config.
type Database struct {
	Host string `json:"host,omitempty" yaml:"host,omitempty" ini:"host,omitempty"`
	Port string `json:"port,omitempty" yaml:"port,omitempty" ini:"port,omitempty"`
	User string `json:"user,omitempty" yaml:"user,omitempty" ini:"user,omitempty"`
	Pwd  string `json:"pwd,omitempty" yaml:"pwd,omitempty" ini:"pwd,omitempty"`
	Name string `json:"name,omitempty" yaml:"name,omitempty" ini:"name,omitempty"`
}

// GetDSN dbconnect tcp setting
func (d Database) GetDSN() string {
	return d.User + ":" + d.Pwd + "@tcp(" + d.Host + ":" + d.Port + ")/" + d.Name + "?parseTime=true"
}

// ReadFromJSON read the Config from a JSON file.
func ReadFromJSON(path string) Database {
	jsonByte, err := ioutil.ReadFile(path)

	if err != nil {
		panic(err)
	}

	var cfg Database

	err = json.Unmarshal(jsonByte, &cfg)

	if err != nil {
		panic(err)
	}

	return cfg
}

// ReadFromYaml read the Config from a YAML file.
func ReadFromYaml(path string) Database {
	jsonByte, err := ioutil.ReadFile(path)

	if err != nil {
		panic(err)
	}

	var cfg Database

	err = yaml.Unmarshal(jsonByte, &cfg)

	if err != nil {
		panic(err)
	}

	return cfg
}
