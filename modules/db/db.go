package db

import (
	"backend/modules/db/config"
	"database/sql"
	"fmt"
	"os"
)

//InitDB connect
func InitDB() *sql.DB {
	// cfg := config.Database{
	// 	Host: "127.0.0.1",
	// 	Port: "3306",
	// 	User: "ycadmin",
	// 	Pwd:  "yi@@##4321",
	// 	Name: "fish888",
	// }
	path, _ := os.Getwd()
	cfg := config.ReadFromYaml(path + "/modules/db/config/config.yaml")
	fmt.Println(os.Getenv("ENV"))
	db, err := sql.Open("mysql", cfg.GetDSN())

	if err != nil {
		_ = db.Close()
		panic(err)
	}
	return db
}

// ToObject Sql queary data
func ToObject(rows *sql.Rows) []interface{} {
	cols, _ := rows.Columns()
	var ret []interface{}
	columns := make([]interface{}, len(cols))
	columnPointers := make([]interface{}, len(cols))
	for rows.Next() {
		for i := range columns {
			columnPointers[i] = &columns[i]
		}
		rows.Scan(columnPointers...)
		data := make(map[string]interface{})
		for i, col := range cols {
			val := columns[i]
			b, ok := val.([]byte)
			var v interface{}
			if ok {
				v = string(b)
			} else {
				v = val
			}
			data[col] = v
		}
		ret = append(ret, data)
	}
	return ret
}

// ToObjectRow Sql queary data
func ToObjectRow(rows *sql.Rows) map[string]interface{} {
	cols, _ := rows.Columns()
	columns := make([]interface{}, len(cols))
	columnPointers := make([]interface{}, len(cols))
	var data = make(map[string]interface{})
	for rows.Next() {
		for i := range columns {
			columnPointers[i] = &columns[i]
		}
		rows.Scan(columnPointers...)

		for i, col := range cols {
			val := columns[i]
			b, ok := val.([]byte)
			var v interface{}
			if ok {
				v = string(b)
			} else {
				v = val
			}
			data[col] = v
		}
	}
	return data
}
